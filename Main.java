public class Main {
    public static void main(String[] args) {
        String[] array = new String[]{"java", "is", "the", "best", "programming", "language"};
        String[] backup = new String[6];
        System.arraycopy(array, 0, backup, 0, array.length);
        SelectionSort.sort(array);
        ArrayPrint.print(array);
        System.arraycopy(backup, 0, array, 0, array.length);
        BubbleSort.sort(array);
        ArrayPrint.print(array);
        System.arraycopy(backup, 0, array, 0, array.length);
        InsertionSort.sort(array);
        ArrayPrint.print(array);
    }
}

class ArrayPrint {
    public static void print(String[] array) {
        for (String element : array) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}

class SelectionSort {
    public static void sort(String[] array) {
        System.out.println("Selection :");
        for (int i = 0; i < array.length; i++) {
            int pos = i;
            String temp;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[pos]) <= 0) {
                    pos = j;
                }
            }
            temp = array[pos];
            array[pos] = array[i];
            array[i] = temp;
        }
    }
}

class BubbleSort {
    public static void sort(String[] array) {
        System.out.println("Bubble :");
        boolean isSorted = false;
        String buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].compareTo(array[i + 1]) >= 0) {
                    isSorted = false;
                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;
                }
            }
        }
    }
}

class InsertionSort {
    public static void sort(String[] array) {
        System.out.println("Insertion : ");
        for (int i = 0; i < array.length; i++) {
            String currElem = array[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && array[prevKey].compareTo(currElem) >= 0) {
                array[prevKey + 1] = array[prevKey];
                array[prevKey] = currElem;
                prevKey--;
            }
        }
    }
}